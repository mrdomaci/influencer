<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Connection;
use App\Models\Social;

class Requirement extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'social_type_id', 'size'
    ];

    public static function check($user, $influencer) {
        $requirements = Requirement::where('user_id', $user->id)->get();
        $passed = false;
        foreach($requirements as $requirement) {
            if($influencer->socials->where('social_type_id', $requirement->social_type_id)->where('size', '>=', $requirement->size)->first()) {
                $passed = true;
                break;
            }
        }
        Connection::create([
            'user_id' => $user->id,
            'influencer_id' => $influencer->id,
            'state' => 'new',
            'passed' => $passed
        ]);
    }
}
