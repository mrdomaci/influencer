<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Email;
use App\Models\Social;

class Influencer extends Model
{
    use HasFactory;
    protected $fillable = [
        'code'
    ];
    public function emails() {
        return $this->hasMany(Email::class);
    }
    public function socials() {
        return $this->hasMany(Social::class);
    }
}
