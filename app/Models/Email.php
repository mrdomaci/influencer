<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Influencer;

class Email extends Model
{
    use HasFactory;
    protected $fillable = [
        'email', 'influencer_id'
    ];

    public function influencer() {
        return $this->belongsTo(Influencer::class);
    }

    public static function generateUniqueCode()
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersNumber = strlen($characters);

        $code = '';

        while (strlen($code) < 6) {
            $position = rand(0, $charactersNumber - 1);
            $character = $characters[$position];
            $code = $code.$character;
        }

        if (Influencer::where('code', $code)->exists()) {
            self::generateUniqueCode();
        }

        return $code;
    }
}
