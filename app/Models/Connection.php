<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Influencer;
use App\Models\Social;
use App\Models\User;
use App\Models\Email;

class Connection extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'influencer_id', 'passed', 'state'
    ];
    public function influencer() {
        return $this->belongsTo(Influencer::class);
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function socials() {
        return $this->hasManyThrough(Social::class, Influencer::class, 'id', 'influencer_id', 'influencer_id', 'id');
    }
    public function emails() {
        return $this->hasManyThrough(Email::class, Influencer::class, 'id', 'influencer_id', 'influencer_id', 'id');
    }
}
