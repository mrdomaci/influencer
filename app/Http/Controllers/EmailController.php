<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewInfluencer;
use App\Models\Email;
use App\Models\Influencer;
use App\Models\Requirement;
use App\Models\Connection;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string'
        ]); 
        $email = Email::where('email', $fields['email'])->first();
        $user = $request->user();
        if($email) {
            $influencer = $email->influencer;
            Requirement::check($user, $influencer);
        }
        else {
            $code = Email::generateUniqueCode();
            $influencer = Influencer::create([
                'code' => $code
            ]);
            Email::create([
                'influencer_id' => $influencer->id,
                'email' => $fields['email']
            ]);
            Connection::create([
                'user_id' => $user->id,
                'influencer_id' => $influencer->id,
                'state' => 'new',
                'passed' => false
            ]);
            return Mail::to($fields['email'])->send(new NewInfluencer($code));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
