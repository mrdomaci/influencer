<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Mail\RegisterLink;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request) {
        if (! $request->hasValidSignature()) {
            abort(401);
        }
        $user = User::where('email', $request['email'])->first();
        if(!$user) {
             $fields = $request->validate([
                'name' => 'required|string', 
                'email' => 'required|string|unique:users,email'
            ]); 
            $user = User::create([
                'name' => $fields['name'],
                'email' => $fields['email'],
                'password' => bcrypt(sha1(rand())),
            ]);
        } else {
            $user->tokens()->where('tokenable_id', $user->id)->delete();
        }
        $token = $user->createToken('myToken')->plainTextToken;
        return view('token', ['token' => $token, 'platform' => $request['platform']]);
    }

    public function sendLink(Request $request) {
        $fields = $request->validate([
            'name' => 'required|string', 
            'email' => 'required|string',
            'platform' => 'required|string'
        ]);
        $url = URL::signedRoute('register', ['email' => $fields['email'], 'name' => $fields['name'], 'platform' => $fields['platform']]);
        Mail::to($fields['email'])->send(new RegisterLink($url));
        if($request['redirect']) {
            return redirect($request['redirect']);
        }
        else {
            return true;
        }
    }
}
