@component('mail::message')
# Získejte výhody upřímnou reklamou

Ověřte svoje soiální sítě odesláním níže uvedeného kódu do zprávy na naše sociální sítě a získáte možnost navázat spolupráci s vašimi oblíbenými eshopy.

@component('mail::panel')
<h1>{{ $code }}</h1>
@endcomponent
<ul>
    <li><a href='https://www.facebook.com/jan.m.slabihoud'>Facebook</a></li>
    <li><a href='https://www.instagram.com/mr.domaci/'>Instagram</a></li>
</ul>

Více o fungování upřímné reklamy se dočtete na <a href="{{ config('app.url') }}">webu</a> nebo v <a href="{{ config('app.url') }}/terms">pravidlech použití</a>.

Přejeme hezký den,<br>
{{ config('app.name') }}
@endcomponent
