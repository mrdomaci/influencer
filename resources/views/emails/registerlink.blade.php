@component('mail::message')
# Doknčení nastavení

Kliknutím na odkaz níže bude pro váš email vygenerován token, kterým budete bezpečně moci přistupovat k datům o influencerech.
Tento token už jen vložte do vašeho eshopu do pole pro token a je hotovo.

@component('mail::button', ['url' => $url])
Získat token
@endcomponent

Přejeme hezký den,<br>
{{ config('app.name') }} tým
@endcomponent
