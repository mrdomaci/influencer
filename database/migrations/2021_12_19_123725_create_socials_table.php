<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->id();
            $table->integer('size');
            $table->bigInteger('social_type_id')->unsigned()->index()->nullable();
            $table->foreign('social_type_id')->references('id')->on('social_types')->onDelete('cascade');
            $table->bigInteger('influencer_id')->unsigned()->index()->nullable();
            $table->foreign('influencer_id')->references('id')->on('influencers')->onDelete('cascade');
            $table->string('profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
