<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConnectionController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/register',[AuthController::class, 'register'])->name('register');
Route::post('/sendlink',[AuthController::class, 'sendlink'])->name('semdlink');

Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::get('/connections', [ConnectionController::class, 'index']);
    Route::post('/email', [EmailController::class, 'store']);
    Route::post('/connection', [ConnectionController::class, 'update']);
});
